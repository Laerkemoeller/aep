# MiniX9
## Flowchart

![Picture of flowchart](MiniX9/flowchart_individual.png "Flowchart")
<br>

This Minix is a flowchart of my previous Minix6 program, which is the one where we had to create a game. I found this one to be the most complex, as it is the program I have made that is the most interactive from the user's perspective. This program also consists of various stages and actions that need to happen based on certain conditions, so I thought it would make the most interesting flowchart as well. I found it difficult to choose what was necessary to include in the flowchart, as it would become way too long if I went through every single step of my code. Making a flowchart for existing code is challenging because you have to look at the code and zoom out to be able to determine what is important to communicate in a flowchart. After creating this flowchart, I gained a better understanding of my own code because it made how the different steps were intertwined more visual and therefore created a better understanding of how the computer interprets the code it is given. I find the flowchart very useful to look at if I ever were to further develop my program or if I were to explain the code to someone else, someone who might not even understand code themselves.
<br>

The individual flowchart is very much retrospective, meaning that I had to look at my code and take a step back to understand how I could communicate it in a flowchart. The group flowcharts are made before the actual program, creating a very different use for them. In the group one, I think it is primarily useful to get a sense of what the program needs to be able to do and to get everyone on the same page about what needs to be written into the code. Therefore, the group flowcharts aren't as precise in code in the sense that it hasn't been decided which syntaxes to use and only give an overview of the idea more than the actual code. The individual flowchart is effective in explaining the code while still trying to communicate the effect of the code in a simple way.
<br>

You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX5)