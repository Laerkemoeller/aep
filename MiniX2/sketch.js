// Variable to track whether glasses are on or off
let glassesOn = true;

// Preloading some images for later use
function preload() {
  blood = loadImage('blood.png');
  glasses = loadImage('glasses.png');
  glasses2 = loadImage('glasses2.png');
  watermelon = loadImage('watermelon.png');
  keyboard = loadImage('keyboard.jpg');
  keyboard2 = loadImage('keyboard2.jpg');
  textleft = loadImage('textleft.png');
  textright = loadImage('textright.png');
}

function setup() {
  createCanvas(1512, 865);
}

function draw() {
  background(200);
  image(textleft, 100, 0);
  image(textright, 890, 0);

  // Checking if glasses are on
  if (glassesOn) {
    // If glasses are on, draw elements with glasses on
    drawGlassesOn();
  } else {
    // If glasses are off, draw elements with glasses off
    drawGlassesOff();
  }

  // Displaying text
  fill(0);
  textSize(17);
  text('Click to remove the rose-colored glasses', width / 2, 30);
  textAlign(CENTER);
}

// Drawing elements with glasses on
function drawGlassesOn() {
  // Emoji to the left
  // Mother hair (back)
  noStroke();
  fill(74, 44, 25);
  ellipse(370, 150, 160, 170);
  // Mother body
  noStroke();
  fill(206, 18, 39);
  rect(270, 190, 200, 200, 90, 90, 20, 20);
  // Mother head
  fill(213, 164, 130);
  ellipse(370, 150, 130);
  // Mother hair (front)
  fill(74, 44, 25);
  rect(360, 85, 70, 40, 0, 40, 90, 40);
  rect(310, 82, 50, 40, 60, 0, 60, 0);
  rect(300, 110, 20, 82);
  rect(420, 110, 20, 82);
  // Mother hands
  fill(213, 164, 130);
  arc(320, 390, 40, 40, PI, 0);
  arc(430, 390, 40, 40, PI, 0);
  // Mother left arm
  push();
  fill(216, 28, 49);
  noStroke();
  translate(290, 290);
  rotate(1.1);
  rect(0, 0, 90, 40, 10);
  rotate(1);
  rect(-50, -21, 90, 40, 10);
  pop();
  // Mother face
  noFill();
  stroke(153, 104, 70);
  arc(370, 180, 30, 15, 0, PI);
  arc(370, 160, 15, 15, 0, PI);
  strokeWeight(6);
  arc(345, 140, 20, 5, PI, 0);
  arc(395, 140, 20, 5, PI, 0);
  // Baby body
  noStroke();
  fill(137, 207, 240);
  push();
  translate(380, 320);
  rotate(-1.4);
  ellipse(0, 0, 100, 180);
  pop();
  // Mother right arm
  push();
  fill(216, 28, 49);
  noStroke();
  translate(415, 370);
  rotate(-1.1);
  rect(0, 0, 90, 40, 10);
  rotate(-0.9);
  rect(20, 55, 90, 40, 10);
  pop();
  // Baby head
  noStroke();
  fill(238, 206, 179);
  ellipse(290, 260, 100);
  // Baby hat
  fill(186, 214, 209);
  push();
  translate(285, 255);
  rotate(-1);
  arc(0, 0, 100, 100, PI, 0);
  fill(152, 190, 191);
  rect(-52.5, -5, 105, 15, 5);
  // Baby eye
  strokeWeight(3);
  noFill();
  stroke(178, 146, 119);
  arc(20, 25, 20, 5, PI, 0);
  pop();
  // Baby mouth
  strokeWeight(3);
  noFill();
  stroke(178, 146, 119);
  arc(332, 270, 10, 5, 0, PI)
  // Emoji to the right
  // Child body
  noStroke();
  fill(213, 164, 130);
  rect(1100, 150, 100, 150, 40);
  // Child Head 
  fill(202, 154, 120);
  ellipse(1150, 120, 95, 100);
  // Child diaper
  push();
  stroke(220);
  fill(255);
  translate(1150, 250);
  rotate(-0.4);
  arc(0, 0, 110, 110, 0, PI + QUARTER_PI, CHORD);
  pop();
  fill(220);
  rect(1175, 240, 30, 20, 40, 3, 3, 40);
  rect(1095, 240, 30, 20, 3, 40, 40, 3);
  // Child Legs
  push();
  translate(1080, 315);
  rotate(-1)
  fill(213, 164, 130);
  rect(0, 0, 60, 40, 40);
  rect(17, 89, 60, 30, 0, 40, 40, 40);
  rect(-35, 55, 40, 25, 40);
  pop();
  push();
  translate(1190, 265);
  rotate(1);
  fill(213, 164, 130);
  rect(0, 0, 60, 40, 40);
  rect(-18, 90, 60, 30, 40, 0, 40, 40);
  rect(53, 50, 40, 25, 40);
  pop();
  // Child arms
  push();
  translate(1055, 197);
  rotate(-0.5);
  fill(213, 164, 130);
  rect(0, 0, 80, 35, 40);
  pop();
  push();
  translate(1176, 160);
  rotate(0.5);
  fill(213, 164, 130);
  rect(0, 0, 80, 35, 40);
  pop();
  fill(213, 164, 130);
  rect(1212, 194, 28, 50, 40);
  rect(1060, 194, 28, 50, 40);
  ellipse(1075, 250, 35);
  ellipse(1225, 250, 35);
  // Child face 
  noFill();
  stroke(153, 104, 70);
  arc(1150, 140, 25, 10, 0, PI);
  arc(1150, 130, 7, 3, 0, PI);
  strokeWeight(3);
  arc(1170, 120, 13, 5, PI, 0);
  arc(1130, 120, 13, 5, PI, 0);
  // Child hat
  noStroke();
  fill(186, 214, 209);
  arc(1150, 100, 100, 100, PI, 0);
  fill(152, 190, 191);
  rect(1097.5, 100, 105, 10, 3);
  // Keyboard
  image(keyboard2, 0, height / 2, width, height / 2);
  // Rose-colored glasses
  noStroke();
  fill(255, 182, 193, 100);
  ellipse(350, 300, 580, 512);
  ellipse(1170, 300, 580, 512);
  image(glasses, 0, 20, width, 550);
}

// Drawing elements with glasses off
function drawGlassesOff() {
  // Emoji to the left
  // Mother hair (back)
  noStroke();
  fill(74, 44, 25);
  ellipse(370, 150, 160, 170);
  // Mother body
  noStroke();
  fill(206, 18, 39);
  rect(270, 190, 200, 200, 90, 90, 20, 20);
  // Mother head
  fill(213, 164, 130);
  ellipse(370, 150, 130);
  // Mother hair (front)
  fill(74, 44, 25);
  rect(360, 85, 70, 40, 0, 40, 90, 40);
  rect(310, 82, 50, 40, 60, 0, 60, 0);
  rect(300, 110, 20, 82);
  rect(420, 110, 20, 82);
  // Mother tears
  fill(127, 206, 242);
  rect(405, 145, 15, 15, 0, 40, 80, 80);
  rect(420, 165, 15, 15, 0, 40, 80, 80);
  rect(315, 145, 20, 20, 40, 0, 80, 80);
  // Mother hands
  fill(213, 164, 130);
  arc(320, 390, 40, 40, PI, 0);
  arc(430, 390, 40, 40, PI, 0);
  // Mother left arm
  push();
  fill(216, 28, 49);
  noStroke();
  translate(290, 290);
  rotate(1.1);
  rect(0, 0, 90, 40, 10);
  rotate(1);
  rect(-50, -21, 90, 40, 10);
  pop();
  // Mother face
  noFill();
  stroke(153, 104, 70);
  strokeWeight(3);
  arc(370, 190, 30, 15, PI, 0);
  arc(370, 160, 15, 15, 0, PI);
  strokeWeight(6);
  arc(345, 140, 20, 5, PI, 0);
  arc(395, 140, 20, 5, PI, 0);
  // Baby body
  noStroke();
  fill(137, 207, 240);
  push();
  translate(380, 320);
  rotate(-1.4);
  ellipse(0, 0, 100, 180);
  pop();
  // Mother right arm
  push();
  fill(216, 28, 49);
  noStroke();
  translate(415, 370);
  rotate(-1.1);
  rect(0, 0, 90, 40, 10);
  rotate(-0.9);
  rect(20, 55, 90, 40, 10);
  pop();
  //Baby head
  noStroke();
  fill(238, 206, 179);
  ellipse(290, 260, 100);
  // Baby hat
  fill(213, 94, 94);
  push();
  translate(285, 255);
  rotate(-1);
  arc(0, 0, 100, 100, PI, 0);
  fill(18, 111, 63);
  rect(-52.5, -5, 105, 15, 5);
  fill(240, 238, 236);
  rect(-52.5, -5, 105, 5, 5);
  image(watermelon, -75, -75, 140, 120);
  pop();
  // Baby face
  strokeWeight(3);
  stroke(178, 146, 119);
  line(320, 240, 320, 260);
  line(310, 250, 330, 250);
  // Baby blood
  image(blood, 330, 270, 95, 95);
  // Emoji to the right
  // Child body
  noStroke();
  fill(213, 164, 130);
  rect(1107.5, 150, 85, 150, 40);
  stroke(193, 144, 110);
  line(1165, 210, 1190, 200);
  line(1165, 220, 1190, 210);
  line(1165, 230, 1190, 220);
  line(1135, 210, 1110, 200);
  line(1135, 220, 1110, 210);
  line(1135, 230, 1110, 220);
  // Child Head 
  noStroke();
  fill(207, 159, 125);
  ellipse(1150, 120, 95, 100);
  // Child diaper
  push();
  stroke(220);
  fill(255);
  translate(1150, 250);
  rotate(-0.4)
  arc(0, 0, 110, 110, 0, PI + QUARTER_PI, CHORD);
  pop();
  fill(220);
  rect(1175, 240, 30, 20, 40, 3, 3, 40);
  rect(1095, 240, 30, 20, 3, 40, 40, 3);
  // Child Legs
  push();
  translate(1080, 315);
  rotate(-1);
  fill(213, 164, 130);
  rect(0, 0, 60, 35, 40);
  rect(17, 89, 60, 25, 0, 40, 40, 40);
  rect(-35, 55, 40, 25, 40);
  pop();
  push();
  translate(1190, 265);
  rotate(1);
  fill(213, 164, 130);
  rect(0, 0, 60, 35, 40);
  rect(-18, 90, 60, 25, 40, 0, 40, 40);
  rect(53, 50, 40, 25, 40);
  pop();
  // Child arms
  push();
  translate(1055, 197);
  rotate(-0.5)
  fill(213, 164, 130);
  rect(0, 0, 80, 30, 40);
  pop();
  push();
  translate(1176, 160);
  rotate(0.5);
  fill(213, 164, 130);
  rect(0, 0, 80, 30, 40);
  pop();
  fill(213, 164, 130);
  rect(1217, 194, 24, 50, 40);
  rect(1060, 194, 24, 50, 40);
  ellipse(1075, 250, 30);
  ellipse(1225, 250, 30);
  // Child face 
  noFill();
  stroke(153, 104, 70);
  arc(1150, 150, 25, 10, PI, 0);
  arc(1150, 130, 7, 3, 0, PI);
  strokeWeight(3);
  arc(1170, 120, 13, 5, PI, 0);
  arc(1130, 120, 13, 5, PI, 0);
  stroke(193, 144, 110);
  line(1175, 140, 1185, 130);
  line(1125, 140, 1115, 130);
  // Child hat
  noStroke();
  fill(213, 94, 94);
  arc(1150, 100, 100, 100, PI, 0);
  fill(18, 111, 63);
  rect(1097.5, 100, 105, 10, 3);
  fill(240, 238, 236);
  rect(1097.5, 98, 105, 5, 3);
  push();
  translate(1090, 15);
  rotate(0.2);
  image(watermelon, 0, 0, 140, 120);
  pop();
  // Keyboard
  image(keyboard, 0, height / 2, width, height / 2);
  // Rose-colored glasses
  noStroke();
  fill(255, 182, 193, 100);
  ellipse(350, 778, 580, 512);
  ellipse(1170, 778, 580, 512);
  image(glasses2, 0, 500, width, 550);
}

// when 'mousePressed()' is triggered, 'glassesOn' is being assigned the opposite boolean value of what it currently holds. 
// This effectively toggles the value of 'glassesOn' from true to false, or from false to true, depending on its current state.
function mousePressed() {
  // Toggle the glasses state when mouse is pressed
  glassesOn = !glassesOn;
}