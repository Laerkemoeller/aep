# MiniX2
## Shift in Perspective

![Picture of still frame emoji](MiniX2/Still_frame.png "Shift in Perspective")
<br>
Please view the full animation [here](https://Laerkemoeller.gitlab.io/aep/MiniX2/emoji.html)
<br>
You can view the code [here](https://gitlab.com/Laerkemoeller/aep/-/blob/main/MiniX2/sketch.js)
<br>
You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX2)
<br>

The code, in its default mode, consists of two fairly common emojis with not much else to them. I have then placed these rose-colored sunglasses on top, which is an attempt to be a little provocative towards the world. The animation occurs once someone clicks anywhere on the screen, whereupon the sunglasses come off and reveal two new emojis. These emojis are essentially the same code but modified to underline the point of the program, which is that children are dying. If the mouse is clicked multiple times, it will toggle between these two states. I tried taking two pretty positive emojis and turning them into something negative by incorporating one of the biggest conflicts at the moment and manifesting this into the emojis. Then, I placed the rose-colored sunglasses as a way of showing how most Western countries are acting towards the situation, as if everything is fine. The watermelon baby hat is a symbol of Palestine, as the watermelon emoji has been used in that context a lot online since the colors from the Palestinian flag are the same as the watermelon. I kinda liked the idea of taking this "additional" meaning of the watermelon emoji and incorporating it in my own emojis.
I also placed many images in the code, and these aren't really necessary for the code to work, but they help convey the meaning.
<br>

In this MiniX, I have mainly focused on gaining a good understanding of various geometric drawing methods. I have experimented with different shapes and how to rotate them and make them behave the way I wanted. I now feel pretty confident in both rotating objects and creating arcs, which troubled me for a while. I tried using the mousePressed function and created two functions myself to represent the two states the window toggles between when the mouse is pressed. All of this works because of the variable I created at the beginning of the code, which tracks whether the glasses are on or off.
<br>

Since I have made emojis that directly concern politics and contemporary problems, this is pretty much integrated directly into the code. I wanted to create something that represented a minority and something that was a current problem. I have tried to stay as neutral as I could regarding the conflict by not making any explicit statements about it besides acknowledging that innocent people and children are dying. I did create the watermelon hats, but this was primarily to illustrate the current situation of innocent Palestinian children being killed. The emojis are obviously not a very great alternative to the usability of the original ones, but that wasn't really the point of my emojis either. The point of my emojis was to place current emojis into a wider social and cultural context and show how many countries have rose-colored goggles on, pretending everything is fine. I tried to create a shift in perspective.

### References:
[How mousePressed works](https://p5js.org/reference/#/p5.Element/mousePressed)
<br>
[How arc works](https://p5js.org/reference/#/p5/arc)
<br>
[How rotate works](https://p5js.org/reference/#/p5/rotate)

