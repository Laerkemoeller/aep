let bigCircleRadius = 400; // Radius of the big circle
let smallCircleRadius = 40; // Radius of the small circle
let smallCircleSpeed = 20; // Speed for the small circle
let smallCircleX, smallCircleY; // Position of the small circle
let angle; // Angle of movement for the small circle
let ropes = []; // Array to store rope objects
let chords = []; // Array to store chord sound files
let currentChordIndex = 0; // Index of the current chord sound file

// Preload function to load sound files
function preload() {
  rickroll = loadSound("sounds/rickroll.MP3"); // Load Rickroll sound
  for (let i = 0; i <= 54; i++) { // Load chord sound files
    chords.push(loadSound(`sounds/r${i}.mp3`));
  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  angle = random(TWO_PI); // Initialize angle randomly
  // initial position of the small circle being in the center of the canvas
  smallCircleX = width / 2
  smallCircleY = height / 2
}

function draw() {
  background(0);
  drawBigCircle(); // Draw the big circle
  updateSmallCircle(); // Update the position of the small circle
  handleCollision(); // Handle collision with the big circle
  drawRopes(); // Draw ropes connecting small circle to the big circle
  drawBigCircleStroke(); // Draw stroke of the big circle
  drawSmallCircle(); // Draw the small circle
  checkSizeOfSmallCircle(); // Handle case of small circle being as big as big circle
}

// Draw the big circle
function drawBigCircle() {
  fill(255);
  ellipse(width / 2, height / 2, bigCircleRadius * 2);
}

// Update the position of the small circle
function updateSmallCircle() {
  let speedX = cos(angle) * smallCircleSpeed;; // Calculate horizontal component of speed
  let speedY = sin(angle) * smallCircleSpeed;; // Calculate vertical component of speed
  // Update position based on velocity components
  smallCircleX += speedX;
  smallCircleY += speedY;
}

// Handle collision with the big circle
function handleCollision() {
  // Calculate distance to center of big circle
  let distance = dist(smallCircleX, smallCircleY, width / 2, height / 2);
  if (distance >= bigCircleRadius - smallCircleRadius) { // If collision occurs
    // Calculate new angle of movement
    angle = atan2(smallCircleY - height / 2, smallCircleX - width / 2);
    // Update position to be on the edge of the big circle
    smallCircleX = width / 2 + cos(angle) * (bigCircleRadius - smallCircleRadius);
    smallCircleY = height / 2 + sin(angle) * (bigCircleRadius - smallCircleRadius);
    // Create variables for x and y coordinates at the point of collision
    let ropeCollisionX = width / 2 + cos(angle) * bigCircleRadius;
    let ropeCollisionY = height / 2 + sin(angle) * bigCircleRadius;
    // Create a new rope object and add it to the array
    ropes.push(new Rope(ropeCollisionX, ropeCollisionY, smallCircleX, smallCircleY));
    // Randomize angle after collision (between -60 degrees to 60 degrees)
    angle += PI + random(-PI / 3, PI / 3); 
    // Increase radius of the small circle
    smallCircleRadius += 1; 
    // Get current chord sound file
    let currentSong = chords[currentChordIndex];
    // Play current chord sound
    currentSong.play(); 
    // Move to next chord sound file and start from the beginning if the length of the array is exceeded
    currentChordIndex = (currentChordIndex + 1) % chords.length;
  }
}

// Draw ropes connecting small circle to the big circle
function drawRopes() {
  for (let i = 0; i < ropes.length; i++) {
    ropes[i].update(smallCircleX, smallCircleY);
    ropes[i].display();
  }
}

// Draw stroke of the big circle
function drawBigCircleStroke() {
  stroke(0);
  strokeWeight(4);
  noFill();
  ellipse(width / 2, height / 2, bigCircleRadius * 2);
}

// Draw the small circle
function drawSmallCircle() {
  fill(0);
  noStroke();
  ellipse(smallCircleX, smallCircleY, smallCircleRadius * 2);
}

// Check to see if the small circle is as big as the big circle
function checkSizeOfSmallCircle() {
  // If the small circle reaches the size of the big circle
  if (smallCircleRadius >= bigCircleRadius) {
    textSize(32);
    fill(255);
    textAlign(CENTER, CENTER);
    text("Congrats, you made it", width / 2, height / 2); // Display this message
    rickroll.play(); // Play rickroll sound
    currentSong.stop(); // Stop playing the chords
  }
}

// Class to represent ropes connecting small circle to the big circle
class Rope {
  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.color = color(random(255), random(255), random(255)); // Random color for rope
  }

  // Update end point of the rope
  update(x, y) {
    this.x2 = x;
    this.y2 = y;
  }

  // Display the rope
  display() {
    strokeWeight(2.5);
    stroke(this.color);
    line(this.x1, this.y1, this.x2, this.y2);
  }
}