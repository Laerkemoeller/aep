# MiniX5
## Generative art

The program has sound, just tap on the screen to unmute it.

![Picture of still frame generative](MiniX5/still_frame_generative.png "Generative art")
<br>
Please view the full animation [here](https://Laerkemoeller.gitlab.io/aep/MiniX5/generative.html)
<br>
You can view the code [here](https://gitlab.com/Laerkemoeller/aep/-/blob/main/MiniX5/sketch.js)
<br>
You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX5)
<br>

For this MiniX, I wanted to recreate something that I often come across on TikTok for some reason. It's a small circle bouncing inside a larger circle. When a collision occurs between the two circles, the small circle changes direction, a string is added that follows it from the point of collision, and the small circle grows slightly larger. Therefore, my program consists of more than two rules: the first rule governs the change of direction, the second rule governs the spawning of a new string, and the third rule governs the growth of the small circle. As time progresses, the small circle accumulates more strings and increases in size. I also incorporated sound effects for when the small circle collides with the larger circle, mainly to mimic the TikToks I've seen and because I wanted to experiment with the sound library. Adding music and sounds turned out to be easier than I initially thought, so I had fun experimenting with different sounds and songs. The mathematics involved are somewhat confusing; it would have been simpler if the circle just bounced within the canvas. However, with some help for the math and watching numerous videos on cosine, sine, and atan2, I managed.
<br>

The rules in my program define the boundaries for the random elements in my code. The angle of the ball is randomized, but within limits so that it always moves in roughly the opposite direction. I set this to a random value between -60 and 60 degrees, which allows for some variation without it just bouncing back and forth within a narrow range. Initially, I had it set between -45 and 45 degrees, but that felt too predictable. Another rule involves placing a new line or rope at the point of collision and at the ball. While this placement isn't inherently random, as it's always where the ball lands and follows its movement, I added random colors to make it visually more interesting. As the ball grows with each collision, the sounds become more frequent since the ball maintains the same speed but has a shorter distance to travel to hit the larger circle. This causes the program to start out slowly, making it difficult to discern the song represented by the chords, but over time, it becomes easier to hear. When the radius of the small circle gets very close to that of the larger circle, it speeds up significantly. To address this, I included an if statement when the small circle is as big as the larger one, causing the sounds to stop to avoid annoyance and instead play the actual song the chords were representing. I chose a black background and made the small circle black because I like how it becomes all black towards the end.
<br>

I appreciate the concept of something being random yet still following a format established by the creator. In my MiniX, the angle is entirely random within the -60 to 60-degree range, but it never exceeds the boundaries I've set. Therefore, the program generates a unique piece each time, but always within the rules I've defined. I considered randomizing the speed of the small circle between certain values, but this interfered with the sound, making it harder to identify the song being played by the chords. If the sounds weren't present, I would have varied the speed upon each collision to make it even more random. Overall, the computer generates something within the constraints I've established, creating a degree of autonomy, yet maintaining a level of control that keeps it somewhat predictable.


### References:
[How atan2 works](https://p5js.org/reference/#/p5/atan2)
<br>
[How dist works](https://p5js.org/reference/#/p5/dist)
<br>
[How cos works](https://p5js.org/reference/#/p5/cos)
<br>
[How sin works](https://p5js.org/reference/#/p5/sin)
<br>
[Visualisation of how cos and sin works](https://editor.p5js.org/yadlra/sketches/nA07Fbksy)
<br>
[Polar coordinates](https://www.youtube.com/watch?v=O5wjXoFrau4)
