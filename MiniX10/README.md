# MiniX10
## Final Project

Please view the full animation [here](https://laerkemoeller.gitlab.io/aep/MiniX10/index.html)
<br>
You can view the code [here](https://gitlab.com/Laerkemoeller/aep/-/blob/main/MiniX10/sketch.js)
<br>
You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX10)
<br>


You can read the readme ![here](MiniX10/ReadMe_-_Final_Project.pdf "PDF")