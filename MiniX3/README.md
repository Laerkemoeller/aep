# MiniX3
## Time is an Illusion

![Picture of still frame emoji](MiniX3/still_frame_throbber.png "Time is an Illusion")
<br>
Please view the full animation [here](https://Laerkemoeller.gitlab.io/aep/MiniX3/throbber.html)
<br>
You can view the code [here](https://gitlab.com/Laerkemoeller/aep/-/blob/main/MiniX3/sketch.js)
<br>
You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX3)
<br>

In this miniX, I have thought a lot about time. I ended up wanting to create something that kind of relates to time as an illusion, and one that might even be man-made. I kept going back to a quote by Albert Einstein: “People like us who believe in physics know that the distinction between past, present, and future is only a stubbornly persistent illusion." This made me create a human stick figure that walks on top of a clock, making it appear that they are the ones making it move. This is the default state of my code, where a human is walking on top of a clock. I then also wanted to create what would happen if the man stopped walking. I created it so that the human figure will stop walking once the mouse is clicked and will start rotating around the clock, as if time now passes the human by. This was to illustrate how time is now an implemented part of the world and even though it might be man-made it is not man-driven. Time has become a huge part of everyone's life and along the way we have become obsessed with time. Time rules our lives. We have structured our world around time. This is also something I thought about while creating my code, that no matter if you try to keep up with time or let time pass you by, time will keep on going. It's not waiting for you to catch up.
<br>

When creating this miniX, I struggled a bit with the demand of loops in the throbber. I found this quite funny since loops aren't necessary for the animation part and that making a throbber without loops is very easy. Loops are only necessary if I wanted to create the same thing over and over again, but I didn't really have any repetitive objects in my code, making it kind of annoying that I had to implement it somewhere in my code. I therefore added the hour indicators on the clock, using a for loop and rotate to make the lines follow along the inside of the stroke of the rounded rectangle. I also created the flickering rectangles in the background with a for loop, just as an extra effect. The real "animation" in my code happens in draw where I primarily use the transformational function rotate, where I add to the angle to create the spinning effect. Draw constantly loops itself which made me wonder why we had to have a for loop or a while loop. This only made sense if I wanted to create a "regular" throbber. I feel like I have mainly added the for loops now for the sake of having them there. To check the requirements off, not because I need them to create something that looks like a throbber.

### References:
[Loop vs. draw](https://www.youtube.com/watch?v=Z8s-7beNP1c)
<br>
[How for loops work](https://p5js.org/reference/#/p5/for)
<br>
[How rotate works](https://p5js.org/reference/#/p5/rotate)
<br>
[How mousePressed works](https://p5js.org/reference/#/p5.Element/mousePressed)
