let legAngle = 0; // Sets the angle of the legs in the beginning
let speedLegs = 2; // Sets the speed of the leg movement
let angleHuman = 0; // Sets the angle of the human
let angleMinute = 0; // Sets the angle of the minute hand
let angleHour = 0; // Sets the angle of the hour hand
let angleClock = 0; // Sets the angle of the clock base
// Variable that indicates whether human is rotating or not
// Is set to false, because human shouldn't be rotating when program starts
let isHumanRotating = false;
// Variable that indicates whether human is finishing rotation or not
// is set to false, because human shouldn't be finishing rotation when program starts
let isHumanFinishingRotation = false; 

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(220);
  // Draw flicker effect in the background
  drawFlicker();
  // Draw clock components
  drawClock();
  drawIndicators();
  drawClockHour();
  drawClockMinute();
  
  // Check if human is rotating or finishing rotation
  if (isHumanRotating || isHumanFinishingRotation) { 
    // Text to display when human is rotaating or finishing rotation
    textAlign(CENTER);
    textSize(20);
    if (isHumanFinishingRotation) {
      text('OK, heading back!', width/2, 50);
    } else {
      text('Click again to start moving...', width/2, 50);
    }
    // Translate to center of canvas and rotate human
    translate(width/2, height/2);
    rotate(angleHuman);
    rotatingHuman();
    // Update rotation angle
    angleHuman += 0.07;
    // Reset rotation angle of human if completing full rotation
    if (angleHuman >= TWO_PI) {
      angleHuman = 0;
      // when isHumanFinishingRotation is true (indicating that the human is finishing their rotation),
      // both isHumanFinishingRotation and isHumanRotating are set to false. 
      // This ensures that the human stops rotating once they have completed their rotation.
      if (isHumanFinishingRotation) {
        isHumanFinishingRotation = false; 
        isHumanRotating = false; 
      }
    }
  } else {
    // Text to display when human is walking and not rotating 
    textAlign(CENTER);
    textSize(20);
    text('Click to stop moving...', width/2, 50);
    translate(width/2, height/2)
    walkingHuman();
  }
}

// Flicker effect in the background
function drawFlicker() {
  push();
  noStroke();
  fill(215);
  // Creates random rectangles to create the flickering
  for (let i = 0; i < 10000; i++) {
    rect(random(width),random(height),random(1,15));
  }
  pop();
}

// Clock base
function drawClock() {
  push();
  translate(width / 2, height / 2);
  rotate(-angleClock);
  stroke(0);
  strokeWeight(8);
  fill(255);
  rectMode(CENTER);
  // Made it a rectangle with smooth edges to be able to see the rotation
  rect(0, 0, 220,220, 105);
  angleClock += 0.1;
  pop();
}

// Clock indicators (Hours) 
function drawIndicators() {
  push();
  translate(width/2, height/2);
  // For loop to not have to create each line 
  for (let i = 0; i < 12; i++) {
    strokeWeight(2);
    rotate(TWO_PI/12);
    line(0,110,0,95);
  } 
  pop();
}

// Minute hand of the clock
function drawClockMinute() {
  push();
  translate(width / 2, height / 2);
  rotate(angleMinute);
  stroke(0);
  strokeWeight(3);
  line(-10, 0, 80, 0)
  // AngleMinute increasing to make it move
  angleMinute += 0.09;
  pop();
}

// Hour hand of the clock
function drawClockHour() {
  push();
  translate(width / 2, height / 2);
  rotate(angleHour);
  stroke(0);
  strokeWeight(4);
  line(0, -10, 0, 50)
  // AngleHour incresing to make it move
  angleHour += 0.02;
  pop();
}

// When human is walking 
function walkingHuman() {
  push();
  rectMode(CENTER);
  // Head
  fill(0);
  ellipse(18, -290, 40, 40);
  // Body
  fill(0);
  rect(0, -237, 25, 80, 10);
  // Legs
  push();
  fill(0);
  translate(0,-200);
  rotate(radians(legAngle));
  rect(0, 40, 20, 80); // Leg
  rect(5, 85, 30, 10, 10) // Foot
  pop();
  push(); 
  fill(0);
  translate(0,-200);
  rotate(-radians(legAngle)); 
  rect(0, 42, 20, 80); // Leg 
  rect(5, 85, 30, 10, 10) // Foot
  pop(); 
  pop();
  // Increments legAngle by speedLegs
  legAngle += speedLegs;
  // Change leg direction when hitting limit
  if (legAngle >= 30 || legAngle <= -30) {
    speedLegs *= -1;
  }
}

// When human is rotating
function rotatingHuman() {
  push();
  rectMode(CENTER);
  // Head
  fill(0);
  ellipse(18, -290, 40, 40);
  // Body
  fill(0);
  rect(0, -237, 25, 80, 10);
  // Legs
  push();
  fill(0);
  translate(0,-200);
  rotate(radians(legAngle));
  rect(0, 40, 20, 80); // Leg
  rect(5, 85, 30, 10, 10) // Foot
  pop();
  push(); 
  fill(0);
  translate(0,-200);
  rotate(-radians(legAngle)); 
  rect(0, 42, 20, 80); // Leg 
  rect(5, 85, 30, 10, 10) // Foot
  pop(); 
  pop();
}

function mousePressed() {
  // Checks if human is rotating or finishing rotating, if this is false, the human will start rotating
  if (!isHumanRotating && !isHumanFinishingRotation) { 
    isHumanRotating = true; 
    // checks if human is rotating and if this is true, the human will finish rotation before stopping. 
  } else if (isHumanRotating) { 
    isHumanFinishingRotation = true; 
  }
}

// Window resizing
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}