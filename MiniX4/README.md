# MiniX4
## You are being watched

![Picture of still frame data](MiniX4/still_frame_data.png "You are being watched")
<br>
Please view the full animation [here](https://Laerkemoeller.gitlab.io/aep/MiniX4/data.html)
<br>
You can view the code [here](https://gitlab.com/Laerkemoeller/aep/-/blob/main/MiniX4/sketch.js)
<br>
You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX4)
<br>

I have created a program that displays a box with terms and conditions. I made the terms and condition text in Chinese, both because they are often unreadable for regular people, but also because people rarely actually read them, so they might as well be in chinese. I made a checkbox that needs to be checked in order to proceed. If the checkbox is not checked and the user presses the button that says 'continue', an alert will pop up and explain that the checkbox needs to be checked to be able to press the continue button. If the user presses the continue button again without having checked the checkbox, a new message will pop up. In total, there are 4 messages that will display infinitely. If the checkbox is checked and the user presses the continue button, the terms and conditions will disappear, and a new box with text will appear. This message is a reality check as to what actually happens once you accept terms and conditions. Meanwhile, a sequence of data will start being gathered in the background. First, a series of images taken from the video capture will appear, 50 in total. Then, an image of the location, obviously just a fake location, but sometimes probably true as the location displayed is at Schön. After this, a bunch of fake data will be displayed, all but the date anyway, which actually displays the real date. Additionally, I made some googly eyes that follow the mouse position and placed the eyes on a picture of Mark Zuckerberg looking through a Windows logo. This was obviously just a fun little additional element, but goes pretty well with the idea of everyone being watched on the internet. I have primarily taken inspiration from Facebook, as they have had some scandals over the years about data capturing. I made it terms and conditions as they give pretty much information, both some that you share yourself and also your behavioral data and how you act online. I also tried to show how services like Facebook actually track you outside of Facebook, getting information about what you might have googled or done on other websites.
<br>

My program visualizes how extensive data capture can be, showing users how their seemingly harmless online activities are constantly monitored and recorded by companies like Facebook. Through my program, I uncover the surveillance mechanisms employed by these platforms, revealing how they track users across various websites and devices, often without explicit consent or awareness. This exposes the comprehensive nature of data capture, where even seemingly unrelated online interactions can be aggregated to create detailed user profiles. By creating a program that critiques data collection practices, I am also raising awareness about the cultural implications of such practices. Users who interact with the program may become more conscious of their online privacy and the broader societal impacts of widespread data capture.
<br>

Some of the cultural implications could be the loss of privacy. The pervasive data capture practiced by companies like Facebook erodes individual privacy, leading to concerns about surveillance, profiling, and the potential for the abuse of personal information. Another implication could be the distribution of power. Data capture often results in a power asymmetry between users and corporations. Companies amass vast amounts of personal data, giving them unprecedented influence over individuals' behaviors, preferences, and even political beliefs. Manipulation and control are another cultural implication of data capture. By analyzing captured data, companies can tailor content, advertisements, and recommendations to manipulate user behavior and opinions. This raises ethical questions about the extent to which individuals are being manipulated without their knowledge or consent. Data capture can even lead to social division and discrimination. Data capture can reinforce existing social divisions and exacerbate discrimination. Algorithms fueled by captured data may inadvertently perpetuate biases, leading to differential treatment based on factors such as race, gender, or socioeconomic status. Overall, my program on data capture contributes to a broader conversation about digital privacy, surveillance, and the societal implications of pervasive online tracking.

### References:
[Getting images from video capture](https://www.youtube.com/watch?v=oLiaUEKsRws&list=PLRqwX-V7Uu6aKKsDHZdDvN6oCJ2hRY_Ig&index=2)
<br>
[How atan2 works](https://www.youtube.com/watch?v=oLiaUEKsRws&list=PLRqwX-V7Uu6aKKsDHZdDvN6oCJ2hRY_Ig&index=2)
<br>
[Googly eyes](https://www.youtube.com/watch?v=oLiaUEKsRws&list=PLRqwX-V7Uu6aKKsDHZdDvN6oCJ2hRY_Ig&index=2)
<br>
[Date formatting](https://stackoverflow.com/questions/25159330/how-to-convert-an-iso-date-to-the-date-format-yyyy-mm-dd)
<br>

