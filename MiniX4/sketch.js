let isCheckboxChecked = false; // Flag to track if the checkbox is checked
let showTACBox = true; // Flag to track if the Terms and Conditions box should be displayed
let continueButton; // Variable to store the Continue button element
let videoCapture; // Variable to store the video capture element
let snapshots = []; // Array to store snapshots captured from video
let eyeLeft, eyeRight; // Variables to store instances of the Eye class
let currentAlertIndex = 0; // Index to track current alert message
// Array of alert messages
let alertMessages = ['You must agree to the terms and conditions to continue.', 'You may not proceed until you agree to the terms and conditions.', 'Are you slow?', 'Just give us your damn consent so we can surveil and control your every move..'];
// Array to store captured data
let capturedData = ['IP Address of device: 192.158.1.38.', '', 'Device model: Apple MacBook', 'Session duration: 4 minutes', 'Preferred language: Danish', 'Searched for "Cheap flights to Greece" ', 'Clicked on 3 ads from clothing brands', 'Studies Digital Design at Aarhus University', 'Used Mastercard xxxx xxxx xxxx 8802 in Føtex', 'Searched for "How to become rich fast"'];

// Get current date and format it and place it at index 1 in the capturedData array
let currentDate = new Date();
capturedData[1] = `Date of activity: ` + currentDate.toLocaleDateString();

// Preload images for later use
function preload() {
  zuckerberg = loadImage('zuckerberg.jpeg'); // Preload image of Zuckerberg (background)
  aarhus = loadImage('location.jpg'); // Preload image of Aarhus (location)
}

function setup() {
  // Set framerate and create canvas
  frameRate(5);
  createCanvas(1512, 858);
  // Setup background and elements
  background(zuckerberg, width, height);
  setupWindowBack();
  setupVideoCapture();
  setupContinueButton();
  setupCheckbox();
  // Create eye objects
  eyeLeft = new Eye(width/1.29, height/2.25, 35);
  eyeRight = new Eye(width/1.215, height/2.25, 35);
}

function draw() {
  // Update and display eye objects
  eyeLeft.update(mouseX, mouseY);
  eyeRight.update(mouseX, mouseY);
  eyeLeft.show();
  eyeRight.show();

  // Conditionally draw elements based on checkbox state
  if (isCheckboxChecked) {
    dataWindowBack();
  }
  // Draw elements
  drawWindowFront();
  drawMessageBox();
  // If showTACBox is true, then it will draw the Terms and Conditions box
  if (showTACBox) {
    drawTACBox();
  }
}

// Setup the window in the background
function setupWindowBack() {
  stroke(80)
  fill(255, 100);
  rect(50, 20, 900, 500, 10);
  noStroke();
  fill(100, 100);
  rect(50, 20, 900, 35, 10, 10, 0, 0)
}

// Draw the window in the front
function drawWindowFront() {
  stroke(150);
  fill(255);
  rect(300, 300, 800, 500, 10);
  noStroke();
  fill(220);
  rect(300, 300, 800, 35, 10, 10, 0, 0)
  stroke(241, 85, 77);
  fill(255, 95, 87)
  ellipse(320, 320, 11);
  stroke(240, 175, 45);
  fill(255, 188, 48)
  ellipse(337, 320, 11);
  stroke(47, 188, 57);
  fill(42, 203, 64);
  ellipse(354, 320, 11);
}

// Draw message box 
function drawMessageBox() {
  fill(200);
  stroke(180);
  rect(500, 400, 400, 300, 10);
  fill(0);
  textSize(15);
  text(`Well, you've done it. You clicked that "Continue" button without a second thought. But now that it's too late, let's talk about what you've really signed up for.`, 510, 420, 380, 300);
  textSize(12);
  text(`Sharing with Strangers: Your data isn't just yours anymore. By blindly agreeing, you've handed it over to third parties. Who are they? What will they do with it? Frankly, it doesn't matter. You've lost control. `, 510, 500, 380, 300);
  text(`Under the Spotlight: Did you think you were invisible online? Think again. Your every move is being watched, analyzed, and probably sold to the highest bidder. Congratulations, you're now a product.`, 510, 560, 380, 300);
  text(`No Turning Back: Sorry, but there's no going back now. You've signed away your rights, and there's no rewind button. From now on, your data is out there, floating in the digital abyss, waiting to be exploited.`, 510, 620, 380, 300);
}

// Draw Terms and Conditions box
function drawTACBox() {
  push();
  fill(7, 102, 255);
  stroke(4, 80, 201);
  rect(500, 400, 400, 300, 10);

  textAlign(CENTER, CENTER);
  textSize(18);
  fill(255);
  text("Terms and Conditions", 700, 430);

  textSize(14);
  text("通过访问本服务/平台，您同意遵守未公开的条款和条件。 您对服务/平台的使用意味着接受其隐秘性质。 您承认服务/平台根据未公开的规定和指南运作。 在此环境中进行的任何活动都受到未公开的协议的约束。 根据秘密惯例，您的个人信息可能会被收集和处理。 在使用服务/平台时，您负责保持谨慎。 您还同意保护服务/平台及其关联方， 免受因您使用而产生的未公开的责任。", 505, 370, 400, 300);
  pop();
}

// Setup video capture
function setupVideoCapture() {
  videoCapture = createCapture(VIDEO); // Create video capture element
  videoCapture.hide(); // Hide video capture element, because it shouldn't be displayed
  snapshots.push(videoCapture.get()); // Capture and store a snapshot from the video
}

// Setup checkbox for agreeing to terms
function setupCheckbox() {
  checkbox = createCheckbox(' I agree to the terms & conditions'); // Create checkbox element
  checkbox.position(575, 605); // Set position of the checkbox
}

// Setup continue button
function setupContinueButton() {
  continueButton = createButton('Continue'); // Create continue button element
  continueButton.mousePressed(continueButtonPressed); // Set event handler for button click
  continueButton.position(665, 640); // Set position of the button
  continueButton.style("background", "#f0a00c"); // Set background color of the button
  continueButton.style("border-radius", "10px"); // Set border radius of the button
  continueButton.style("color", "white"); // Set text color of the button
  continueButton.style("border-color", "#f0a00c"); // Set border color of the button
}

// Handle actions after continue button is pressed
function continueButtonPressed() {
  if (checkbox.checked()) { // If the checkbox is checked
    isCheckboxChecked = true; // Set checkbox flag to true
    showTACBox = false; // Hide Terms and Conditions box
    checkbox.remove(); // Remove checkbox element
    continueButton.remove(); // Remove continue button element
    takeSnapshots(); //Take snapshots
  } else { // If the checkbox is not checked
    alert(alertMessages[currentAlertIndex]); // Show alert message
    currentAlertIndex = (currentAlertIndex + 1) % alertMessages.length; // Increment alert index and makes sure it never goes beyond the length of the array, but restarts
  }
}

let w = 53; // Width of each image tile
let h = 40; // Height of each image tile
let x = 0; // Initial x-coordinate for image tiles
let y = 0; // Initial y-coordinate for image tiles
let cols = 5; // Number of columns of image tiles
let rows = 10; // Number of rows of image tiles
let total = 50; // Total number of image tiles

// Function to draw the captured data in the window in the background
function dataWindowBack() {
  for (let i = 0; i < snapshots.length; i++) {
    fill(50); // Set fill color
    noStroke(); // Disable stroke
    text('Gathering data on user 7864389362 and sharing with third parties...', 60, 90); // Display text message
    imageVideoData(); // Draw image from video capture
    x += w; // Increment x-coordinate with the width of the image
    if (x >= w * cols) { // if x is greater or equal to the width of an image * number of columns
      x = 0; // Reset x-coordinate
      y += h; // Increment y-coordinate with the height of the image
      if (y >= h * rows) { // if y is greater or equal to the height of an image * number of rows
        if (snapshots.length = total) { // Check if all snapshots are captured
          snapshots = []; // Clear snapshots array
          x = 0; // Reset x-coordinate
          y = 0; // Reset y-coordinate
          setTimeout(imageLocationData, 2000); // Call function to draw location data after delay on 2 seconds
          setTimeout(textData, 4000); // Call function to draw text data after delay on 4 seconds
        }
      }
    }
  }
}

// Function to draw image from video capture
function imageVideoData() {
  push();
  tint(255, 255, 255, 150); // Set image tint to make somewhat transparent
  translate(60, 100); // Translate to position image
  image(videoCapture, x, y, w, h, 20, 20); // Display current image from the video capture
  pop();
}

// Function to draw location data
function imageLocationData() {
  push();
  tint(255, 255, 255, 100); // Set image tint to make somewhat transparent
  image(aarhus, 335, 100, 297.5, 200); // Display location image
  pop();
}

// Function to draw text data
function textData() {
  textAlign(LEFT);
  noStroke(); // Disable stroke
  // Loop through each element in the capturedData array
  for (let j = 0; j < capturedData.length; j++) {
    // The text is drawn at x-coordinate 645 and y-coordinate calculated based on index 'j'
    // Each subsequent text is drawn 20 pixels below the previous one
    text(capturedData[j], 645, 110 + j * 20); // Display captured data
  }
}

// Eye class definition
class Eye {
  constructor(tx, ty, ts) {
    this.x = tx; // x-coordinate of eye
    this.y = ty; // y-coordinate of eye
    this.size = ts; // Size of eye
    this.angle = 0; // Angle of eye rotation
  }

  update(mx, my) {
    this.angle = atan2(my - this.y, mx - this.x); // Update angle based on mouse position
  }

  show() {
    push();
    translate(this.x, this.y); // Translate to eye position
    fill(255); // Set fill color
    noStroke(); // Disable stroke
    ellipse(0, 0, this.size, this.size); // Draw eye
    rotate(this.angle); // Rotate pupil based on angle
    fill(0); // Set fill color
    ellipse(this.size / 4, 0, this.size / 3, this.size / 3); // Draw pupil
    pop();
  }
}