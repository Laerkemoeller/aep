// declares an empty array, where all the information about each rectangle in the grid will be stored
let rectangles = [];
// defines a variable named rectSize and is what decides how big the rectangles are
let rectSize = 72.1;

// preloading the image 'Pepita.jpeg', that way it can be used further down in the code
function preload() {
  pepita = loadImage('Pepita.jpeg');
}

// loops through the canvas width and height in steps of rectSize to create a grid of rectangles
function setup() {
  createCanvas(windowWidth, windowHeight);
  for (let x = 0; x < width; x += rectSize) {
    for (let y = 0; y < height; y += rectSize) {
      // creates a new Rectangle object at each grid position and adds it to the rectangles array
      let r = new Rectangle(x, y, rectSize, rectSize);
      rectangles.push(r);
    }
  }
}

// checks if the mouse is being dragged over any of the rectangles
// if so, removes that rectangle from the array
function mouseDragged() {
  for (let i = rectangles.length - 1; i >= 0; i--) {
    if (rectangles[i].contains(mouseX, mouseY)) {
      rectangles.splice(i, 1);
    }
  }
}

function draw() {
  // The image acts as background and only gets shown once grid gets removed
  image(pepita, 0, 0, width, height);
  // Text that only gets shown once grid gets removed
  fill('white');
  textSize(20);
  text("Oh, es Pepita.", 1300, 500);
  text("¿O es Pepona?", 1330, 530);

  // loops through the rectangles array and displays each rectangle
  for (let i = 0; i < rectangles.length; i++) {
    rectangles[i].updateColor(); // Updates the color of each rectangle
    rectangles[i].show(); // Makes the rectangles visible
  }

  // Text that gets shown on top of the grid
  textSize(102);
  text('Wanna get the madness to stop?', 0, 80);
  textSize(60);
  text('Just drag the mouse', 50, 140);
}

//Class defining what it means to be a rectangle
class Rectangle {
  constructor(x, y, w, h) {
    this.x = x; // x-coordinate of the rectangle
    this.y = y; // y-coordinate of the rectangle
    this.w = w; // width of the rectangle
    this.h = h; // height of the rectangle
    this.updateColor(); // Initializes the color of the rectangle
  }

  // How the colors of the rectangle with random RGB values needs to update
  updateColor() {
    this.fillColor = color(random(255), random(255), random(255));
  }

  // What the rectangles should look like
  show() {
    fill(this.fillColor);
    noStroke();
    rect(this.x, this.y, this.w, this.h);
  }

  // Checks if a given point (px, py) is inside a rectangle
  contains(px, py) {
    return px >= this.x && px <= this.x + this.w && py >= this.y && py <= this.y + this.h;
  }
}