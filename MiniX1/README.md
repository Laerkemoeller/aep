# MiniX1
## Madness - It's a mad world 

![Picture of still frame madness](MiniX1/MadnessExample.png "It's a mad world")
<br>
Please view the full animation [here](https://laerkemoeller.gitlab.io/aep/MiniX1/madness.html)
<br>
You can view the code [here](https://gitlab.com/Laerkemoeller/aep/-/blob/main/MiniX1/sketch.js)
<br>
You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX1)
<br>

I always find it incredibly challenging to decide what to create when there are absolutely no limitations on what can be done. I aimed to develop something mildly annoying, and I believe I succeeded in that endeavor. Each time I've run the code, I've felt somewhat stressed and unable to focus for extended periods. In essence, I've created my own worst nightmare - a state of madness.
<br>

I have created a grid of rectangles that constantly changes color. The only way to get it to stop is by pressing down the mouse and dragging it across the rectangles. By doing this it will remove the rectangles and reveal what is underneath it all.
<br>

Initially, I designed a grid of rectangles (at this point much smaller, only about 10x10) that changed colors, employing a rather simple code structure. However, I soon realized that this was extremely harsh on the eyes and couldn't be looked at for long. It was absolutely torture for the brain. Therefore, I wanted to provide the person behind the computer the opportunity to get rid of it themselves. I found this concept intriguing, as I think it kinda serves as a metaphor for the idea that to effect change, one must first initiate it, even in small ways. I particularly enjoyed the idea of users being able to exert control over the chaos, enabling them to eliminate it at will.
<br>

I struggled to decide what to place underneath the grid of rectangles that would appear once the rectangles had been removed. I contemplated addressing real-world issues that incite anger, such as the killing of children in Gaza. Alternatively, I considered incorporating something positive, to transition from chaos to cosmos. It could also just have been white. Ultimately, I went with a picture of Pepita, as I am sure she has made us all crazy in trying to figure her out not that long ago. However, this content is easily modifiable and adaptable to suit any context. I chose Pepita to infuse a sense of lightheartedness and fun into the project.
<br>

In this MiniX, my main focus has been to have some fun with the many opportunities the p5 library offers for free. I've been watching many of The Coding Train's videos to grasp the possibilities available in p5.js. In particular, I drew inspiration from his video about removing objects from an array. This is an aspect we've already become familiar with in the fundamentals, so I felt somewhat comfortable taking it on. His video involves removing bubbles when clicked, and I wanted to do something similar, so in many ways, my code closely resembles his. I chose to use the mouseDragged function instead, as it was more suitable for what I wanted to achieve. Additionally, I had to make significant modifications to the code since his involves circles while mine features rectangles, and his circles are randomly placed and constantly moving, whereas mine had to form a grid. Trying to get everything to work has been a challenge, especially considering the crucial order in the draw function, which has troubled me from time to time. Nonetheless, I've had a lot of fun experimenting with it and can't wait to learn more.
<br>

### References:
[How to make a grid of shapes](https://openprocessing.org/sketch/858175/)
<br>
[How to remove objects from arrays](https://www.youtube.com/watch?v=tA_ZgruFF9k&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=32)
<br>
[How to preload images](https://p5js.org/reference/#/p5/preload)
