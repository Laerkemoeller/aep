let eyeleft, eyeright;
let angle = 0;
let img;
let bubbles = [];

function preload() {
  bee = loadImage('bee.gif');
}

function setup() {
 // put setup code here
 createCanvas(windowWidth, windowHeight);
 noCursor()

 eyeleft = new Eye(600, 700, 200);
 eyeright = new Eye(900, 700, 200);

 for (let i = 0; i < 10; i++) {
  let x = random(width);
  let y = random(0, 500);
  let r = random(50, 100);
  let b = new Bubble(x, y, r);
  bubbles.push(b);
}
}

function draw() {
  background(60, 120, 235);

  noStroke();
  fill(56,200,77);
  ellipse(width/2,900,width/1.5, 800);


  eyeleft.update(mouseX, mouseY);
  eyeright.update(mouseX, mouseY);
  eyeleft.show();
  eyeright.show();

  for (let i = 0; i < bubbles.length; i++) {
    if (bubbles[i].contains(mouseX, mouseY)) {
      bubbles[i].changeColor(color(222,193,238,100));
    } else {
      bubbles[i].changeColor(color(231,254,255,100));
    }
    bubbles[i].move();
    bubbles[i].show();
  }

  rectMode(CENTER);
  image(bee, mouseX-50, mouseY-50, 100, 100);
}

class Eye {
  constructor(tx, ty, ts) {
    this.x = tx;
    this.y = ty;
    this.size = ts;
    this.angle = 20;
  }

  // updating where the mouse is located
  update(mx, my) {
    this.angle = atan2(my - this.y, mx - this.x);
  }

  show() {
    push();
    translate(this.x, this.y);
    fill(255);
    ellipse(0, 0, this.size, this.size);
    rotate(this.angle);
    fill(0);
    ellipse(this.size / 4, 0, this.size / 2, this.size / 2);
    pop();
  }
}

function mouseClicked() {
  for (let i = bubbles.length - 1; i >= 0; i--) {
    if (bubbles[i].contains(mouseX, mouseY)) {
      bubbles.splice(i, 1);
      let x = random(width); // Generate random x position
      let y = random(0, 500); // Generate random y position
      let r = random(50, 100); // Generate random radius
      let b = new Bubble(x, y, r); // Create a new bubble
      bubbles.push(b); // Add the new bubble to the array
    }
  }
}

class Bubble {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.brightness = 0;
  }

  changeColor(color) {
    this.color = color;
  }

  contains(px, py) {
    let d = dist(px, py, this.x, this.y);
    if (d < this.r) {
      return true;
    } else {
      return false;
    }
  }

  move() {
    this.x = this.x + random(-2, 2);
    this.y = this.y + random(-2, 2);
  }

  show() {
    stroke(255,50);
    strokeWeight(4);
    fill(this.color);
    ellipse(this.x, this.y, this.r * 2);
  }
}
