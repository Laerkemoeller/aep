# MiniX6
## Apple Catcher

![Picture of still frame game](MiniX6/still_frame_game.png "Apple Catcher")
<br>
Please view the full animation [here](https://Laerkemoeller.gitlab.io/aep/MiniX6/game.html)
<br>
You can view the code [here](https://gitlab.com/Laerkemoeller/aep/-/blob/main/MiniX6/sketch.js)
<br>
You can view the full repository [here](https://gitlab.com/Laerkemoeller/aep/-/tree/main/MiniX6)
<br>

This MiniX is a game about catching falling apples from a tree. Each round lasts 30 seconds, and the goal is to catch as many apples as possible. You control it by using the left and right arrows to move the basket at the bottom of the screen. Apples will start falling from the tree, both good and bad apples. The bad apples will be deducted from the total amount of good apples, leaving you with a score that determines what message will be displayed. When the time is up and the game is over, you can hit the 'try again' button, essentially reloading the page and allowing you to try to beat your score.
<br>

The objects used in my MiniX are the apples and the basket. Both the basket class and the apple class have the properties 'pos' and 'size' in the constructor, which are initialized immediately when I create a new instance of that class. The apple class also has 'speed' as a property in the constructor method, as the apples have to move as well, but unlike the basket they have to move randomly. In both the apple and the basket class, I have created a method called 'show' to display what they look like in my game. For this, I used images to make it easier for myself and create a more realistic-looking game. The apple class also has a method called 'move,' where I can control how the apples need to move. In both classes, I have chosen to make a subclass to essentially separate good and bad apples, and with the basket, it is to create the illusion that the apple actually goes into the basket. This is just two images of the same basket, but the front image of the basket doesn't have that inner part of the basket anymore. This wasn't really necessary, but I feel like it looks better and creates a better visual, even though it is kinda fake. In the code, I then create new object instances of the basket class in setup and use the method 'show' in draw to display the basket, both the front and back of the basket. I use the left and right arrow keys to control the 'pos x' property to make it move from side to side. The good and bad apples I store in an array where I keep pushing new apples to maintain the minimum requirement of apples displayed on the screen. I also use the 'move' and 'show' methods and use the 'splice' method every time an apple either falls into the basket or falls out of the screen.
<br>

I have used classes in a bunch of MiniX's now, and I am really liking classes as they make iteration much easier and with fewer lines of code. This idea of a template where you can create new object instances from this blueprint of how an object like this should behave makes creating multiple of the same object very easy. I also made a class for the basket even though I am only creating one instance of it, but this was just to make it easier to create the effect of the front and back of the basket. It is the first time I try making subclasses/inheritance in classes, which honestly works really well when something you want to create only deviates a tiny bit from the class you already have. I was unsure whether the 'show' method, which is the one that is repeated in the subclass in both the basket and apple class, would overrule the method in the superclass and was pleasantly surprised that it did and therefore didn't end up displaying two images where I only wanted one.

### References:
[Where I got my font from](https://www.dafont.com/hello-day.font)
<br>
[How loadFont works](https://p5js.org/reference/#/p5/loadFont)
<br>
[How keyIsDown works](https://p5js.org/reference/#/p5/keyIsDown)
<br>
[How inheritance/class extension/subclasses work](https://p5js.org/examples/objects-inheritance.html)
<br>
[How reloading the page works](https://www.w3schools.com/jsref/met_loc_reload.asp)
<br>
[How createVector works](https://p5js.org/reference/#/p5/createVector)
<br>
[Where I got inspiration for the timer](https://editor.p5js.org/denaplesk2/sketches/S1OAhXA-M)
