// Arrays to store good and bad apples
let goodApples = [];
let badApples = [];
// Minimum number of good and bad apples on the screen
let minGoodApple = 4;
let minBadApple = 2; 
let basket; // Represents basket
let basketFront; // Represents front of basket
let goodApplesCaught = 0; // Counter for good apples 
let badApplesCaught = 0; // Counter for bad apples
let timer = 30; // 30 second timer
let stage = 0; // Stage in the game
let score; // Total score

function preload() {
  tree = loadImage('tree.png');
  healthyApple = loadImage('applehealthy.png');
  rottenApple = loadImage('applerotten.png');
  basketBack = loadImage('basketback.png');
  frontBasket = loadImage('basketfront.png');
  info = loadImage('info.png');
  catching = loadSound('catchinggood.mp3');
  badcatching = loadSound('catchingbad.mp3');
  myFont = loadFont('HelloDay.otf');
}

// Setup function to initialize canvas and objects
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(tree, width, height)
  basket = new Basket();
  basketFront = new BasketFront();
}

// Draw function responsible for game logic
function draw() {
  // Check the current stage of the game and execute corresponding actions
  if (stage == 0) {
    infoScreen();
  }
  if (keyIsPressed === true) {
    stage = 1;
  }
  if (stage == 1) {
    background(tree, width, height)
    basket.show();
    showBadApples();
    showGoodApples();
    basketFront.show();
    keys();
    checkGoodAppleNum();
    checkBadAppleNum();
    checkRemoveGoodApple();
    checkRemoveBadApple();
    displayScore();
    checkResult();
  }
}

// Function to handle keyboard input for basket movement
function keys() {
  if (keyIsDown(LEFT_ARROW)) {
    basket.pos.x -= 15;
    basketFront.pos.x -= 15;
  }
  if (keyIsDown(RIGHT_ARROW)) {
    basket.pos.x += 15;
    basketFront.pos.x += 15;
  }
}

// Function to display the game's introduction screen
function infoScreen() {
  push();
  imageMode(CENTER);
  image(info, width / 2, height / 2, 1000, 1000);
  textFont(myFont, 30)
  textAlign(CENTER);
  fill(255, 72, 72);
  text('Welcome to Apple Catcher!', width / 2, height / 2 - 100)
  textSize(20);
  fill(0);
  text("Help Grandma bake her famous apple pies by catching as many apples as you can! You have 30 seconds to grab the juicy, ripe apples falling from the tree. But watch out for the rotten ones, they'll deduct points from your score!", width / 2 - 300, height / 2 - 60, 600, 500);
  textSize(25);
  text('How to Play:', width / 2, height / 2 + 30)
  textSize(20);
  text('- Use the left and right arrow keys to move the basket.', width / 2, height / 2 + 60)
  text('- Catch as many good apples as you can to earn points.', width / 2, height / 2 + 80)
  text('- Avoid catching the bad apples to prevent losing points.', width / 2, height / 2 + 100)
  text("- Grandma's counting on you to fill her basket and bake delicious pies!", width / 2, height / 2 + 120)
  textSize(25);
  fill(255, 72, 72);
  text('Are you ready to become the ultimate Apple Catcher?', width / 2, height / 2 + 170)
  text('Tap any key to start catching', width / 2, height / 2 + 200)
  pop();
}

// Function to check and add good apples as needed
function checkGoodAppleNum() {
  if (goodApples.length < minGoodApple) {
    goodApples.push(new Apple());
  }
}

// Function to check and add bad apples as needed
function checkBadAppleNum() {
  if (badApples.length < minBadApple) {
    badApples.push(new BadApple());
  }
}

// Function to display and move good apples
function showGoodApples() {
  for (let i = 0; i < goodApples.length; i++) {
    goodApples[i].show();
    goodApples[i].move();
  }
}

// Function to display and move bad apples
function showBadApples() {
  for (let i = 0; i < badApples.length; i++) {
    badApples[i].show();
    badApples[i].move();
  }
}

// Function to check and remove caught or fallen good apples
function checkRemoveGoodApple() {
  for (let i = goodApples.length - 1; i >= 0; i--) {
    // Check if the bottom of the good apple is below the top of the basket
    if (goodApples[i].pos.y + goodApples[i].size / 2 > height - (basket.size.h - 100)) {
      // Check if the good apple is within the horizontal bounds of the basket
      if (goodApples[i].pos.x + (goodApples[i].size/2) >= basket.pos.x && goodApples[i].pos.x + (goodApples[i].size/2)  <= basket.pos.x + basket.size.w) {
        // Remove the good apple if it's caught by the basket
        goodApples.splice(i, 1);
        catching.play(); // Sound to play when good apple is caught
        goodApplesCaught++; // Increasing number of good apples caught
      } else if (goodApples[i].pos.y > height) {
        // Remove the good apple if it falls out of the screen
        goodApples.splice(i, 1);
      }
    }
  }
}
  
// Function to check and remove caught or fallen bad apples
function checkRemoveBadApple() {
  for (let i = 0; i < badApples.length; i++) {
    // Check if the bottom of the bad apple is below the top of the basket
    if (badApples[i].pos.y + badApples[i].size / 2 > height - (basket.size.h - 100)) {
      // Check if the bad apple is within the horizontal bounds of the basket
      if (badApples[i].pos.x + (goodApples[i].size/2)  > basket.pos.x && badApples[i].pos.x + (goodApples[i].size/2)  < basket.pos.x + (basket.size.w)) {
        // Remove the bad apple if it's caught by the basket
        badApples.splice(i, 1);
        badcatching.play(); // Sound to play when bad apple caught
        badApplesCaught++; // Increasing number of bad apples caught
      } else if (badApples[i].pos.y > height) {
        // Remove the bad apple if it falls out of the screen
        badApples.splice(i, 1);
      }
    }
  }
}

// Function to display player's score and remaining time
function displayScore() {
  fill(255);
  textFont(myFont, 25)
  text('Seconds left: ' + timer, 10, 25)
  text('Apples caught: ' + goodApplesCaught, 10, 45);
  text('Bad Apples caught: ' + badApplesCaught, 10, 65);

  if (frameCount % 60 == 0) {
    timer--; // Increment startNumber every 60 frames (1 second)
  }
}

// Function to check game result when the timer runs out + displaying score and button to restart
function checkResult() {
  if (timer < 0) {
    push();
    imageMode(CENTER);
    image(info, width / 2, height / 2, 1000, 1000);
    pop();
    fill(0);
    textFont(myFont, 60)
    textAlign(CENTER);
    text("TIME'S UP", width / 2, height / 2 - 60)
    let score = goodApplesCaught - badApplesCaught
    textSize(50);
    text('Score: ' + score, width / 2, height / 2);
    // A bunch of different text displayed depending on the final score
    if (score >= 38) {
      fill(255, 72, 72);
      textSize(20);
      text("Absolutely outstanding! You've earned the title of Apple Catching Master!", width / 2, height / 2 + 50)
    } else if (score >= 31 && score <= 37) {
      fill(255, 72, 72);
      textSize(22);
      text("Fantastic job! Grandma's pies are going to be the talk of the town!", width / 2, height / 2 + 50);
    } else if (score >= 21 && score <= 30) {
      fill(255, 72, 72);
      textSize(22);
      text("Well done! Grandma's going to be delighted with this harvest.", width / 2, height / 2 + 50);
    } else if (score >= 11 && score <= 20) {
      fill(255, 72, 72);
      textSize(22);
      text("Nice effort! Grandma's pies will have a de cent amount of apples.", width / 2, height / 2 + 50);
    } else if (score >= 0 && score <= 10) {
      fill(255, 72, 72);
      textSize(22);
      text("Hmm, maybe Grandma's pies will have to wait a little longer.", width / 2, height / 2 + 50);
    }
    button = createButton('TRY AGAIN');
    button.position(width / 2 - 150, height / 2 + 100);
    button.size(300, 100);
    button.style('background', '#7b9e42');
    button.style("font-size", "60px")
    button.style("border-radius", "30px");
    button.style("color", "white"); 
    button.style("font-family", "marker felt ")
    button.style("border-color", "#447328");
    button.mousePressed(reloadCanvas); // Reloading page when mouse is pressed
    function reloadCanvas() { // Function to reload page 
      window.location.reload(true);
    }
    noLoop();
  }
}

// Class representing the basket
class Basket {
  constructor() {
    // Initialize basket position and size
    this.pos = createVector(width / 2);
    this.size = { w: 180, h: 134 }
  }

  show() {
    // Display the basket on the screen
    image(basketBack, constrain(this.pos.x, 0, width - this.size.w), height - (this.size.h - 60), this.size.w, this.size.h);
  }
}

// Subclass of Basket representing the front of the basket
class BasketFront extends Basket {
  show() {
    // Display the front of the basket on the screen
    image(frontBasket, constrain(this.pos.x, 0, width - this.size.w), height - (this.size.h - 60), this.size.w, this.size.h);
  }
}

// Class representing an apple
class Apple {
  constructor() {
    // Initialize apple's position, size, and speed
    this.speed = random(3, 8);
    this.pos = new createVector(floor(random(100, width - 100)), floor(random(0, height / 4)));
    this.size = 50;
  }

  move() {
    // Move the apple downward on the screen
    this.pos.y += this.speed
  }

  show() {
    // Display the good apple on the screen
    image(healthyApple, this.pos.x, this.pos.y, this.size, this.size);
  }
}

// Subclass of Apple representing a bad apple
class BadApple extends Apple {
  show() {
    // Display the bad apple on the screen
    image(rottenApple, this.pos.x, this.pos.y, this.size, this.size);
  }
}